//TennisApp
// - getScore(player)//A,B
// - echo() //return 
//0-0  =  "LOVE-LOVE", 
//15-0 =  "Fifteen-LOVE"
//30-0 =  "Thirty-LOVE"
//40-0 =  "Fourty-LOVE"
//Player A win
class TennisApp {
    constructor() {
       scoreA=0
    }
    echo(){
        return scoreA 
    }
    getScore(player){
        if (player === 'A') this.scoreA+=15
        
    }
}

describe('TennisApp', () => {
    it('should return "LOVE - LOVE" when call echo() at game start', () => {
        //Arrange
        const app = new TennisApp()
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('LOVE - LOVE')
    })
    it('should echo "Fifteen - LOVE" when playerA get first score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Fifteen - LOVE')
    })
    it('should echo "Thirty - LOVE" when playerA get seccond score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Thirty - LOVE')
    })
    it('should echo "Thirty - Thirty" when playerB get seccond score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        app.getScore('B')
        app.getScore('B')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Thirty - Thirty')
    })
    it('should echo "PlayerB WIN" when playerB get forth score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        app.getScore('B')
        app.getScore('B')
        app.getScore('B')
        app.getScore('B')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('PlayerB WIN')
    })
})